from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
# from django.conf.urls import handler404
# from django.shortcuts import redirect


# def redirect_to_home(request, exception=None):
#     return redirect('home')


urlpatterns = [
    path('admin/', admin.site.urls),
    path("", include("event_site.urls")),
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# urlpatterns += [
#     path('', redirect_to_home, name='handler404'),
# ]

# handler404 = 'anime_central_app.urls.redirect_to_home'
