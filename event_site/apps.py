from django.apps import AppConfig


class EventSiteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'event_site'
