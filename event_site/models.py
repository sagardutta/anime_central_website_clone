from django.db import models
from django.contrib.auth.models import AbstractUser
from .manager import UserManager


class User(AbstractUser):
    # id = models.AutoField(primary_key=True)
    username = None
    profile_pic = models.ImageField(
        upload_to="profile/", null=True, blank=True)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.EmailField(unique=True)
    address = models.TextField(null=True, blank=True)
    phone = models.CharField(max_length=10, null=True, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    user_bio = models.TextField(null=True, blank=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()


class Event(models.Model):
    id = models.AutoField(primary_key=True)
    host_id = models.TextField()
    venue = models.TextField()
    start_date = models.DateField()
    end_date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    post_date = models.DateField()
    no_of_people_registered = models.IntegerField(null=True, blank=True)
    max_people_limit = models.IntegerField(null=True, blank=True)
    event_type = models.CharField(max_length=150, null=True, blank=True)
    description = models.TextField()
    theme1 = models.CharField(max_length=150, null=True, blank=True)
    theme2 = models.CharField(max_length=150, null=True, blank=True)
    theme3 = models.CharField(max_length=150, null=True, blank=True)
    event_poster = models.ImageField(
        upload_to="poster/", null=True, blank=True)
    event_name = models.CharField(max_length=150, null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.no_of_people_registered == '':
            self.no_of_people_registered = None
        if self.max_people_limit == '':
            self.max_people_limit = None
        super().save(*args, **kwargs)


class Event_activities(models.Model):
    id = models.AutoField(primary_key=True)
    event_id = models.ForeignKey(Event, on_delete=models.CASCADE)
    activity_name = models.CharField(max_length=150)
    description = models.TextField()
    start_date = models.DateField()
    end_date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()


class Ticket_tiers(models.Model):
    id = models.AutoField(primary_key=True)
    event_id = models.ForeignKey(Event, on_delete=models.CASCADE)
    ticket_price = models.FloatField()
    perks = models.TextField()


class Ticket(models.Model):
    id = models.AutoField(primary_key=True)
    event_id = models.ForeignKey(Event, on_delete=models.PROTECT)
    user_id = models.ForeignKey(User, on_delete=models.PROTECT)
    user_role = models.CharField(max_length=150)
    description = models.TextField()
    perks = models.TextField()


class Blog(models.Model):
    id = models.AutoField(primary_key=True)
    heading = models.CharField(max_length=150)
    date = models.DateField()
    img = models.ImageField(upload_to="blogs/", null=True, blank=True)
    text = models.TextField()
    link = models.TextField(null=True, blank=True)
    tag1 = models.CharField(max_length=150, null=True, blank=True)
    tag2 = models.CharField(max_length=150, null=True, blank=True)
    tag3 = models.CharField(max_length=150, null=True, blank=True)
    tag4 = models.CharField(max_length=150, null=True, blank=True)
    tag5 = models.CharField(max_length=150, null=True, blank=True)


# class Admin(models.Model):
#     id = models.AutoField(primary_key=True)
#     user_id = models.ForeignKey(User, on_delete=models.CASCADE)
