from django.shortcuts import render, redirect, get_object_or_404
from .forms import RegisterForm
from .models import User, Event, Blog
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from itertools import chain
from datetime import date


def home(request):
    return render(request, 'event_site/home.html')


def updates(request):
    events = Event.objects.all().order_by('-id')
    blogs = Blog.objects.all().order_by('-id')

    # To generate mixed list of events and blogs depending on date of posting
    combined_data = list(chain(events, blogs))
    sorted_data = sorted(combined_data, key=lambda x: x.post_date if hasattr(
        x, 'post_date') else x.date)
    print(sorted_data)

    return render(request, 'event_site/updates.html', {"events": events, "blogs": blogs})


def register(request):
    if request.method == 'POST':
        registerForm = RegisterForm(request.POST, request.FILES)

        if registerForm.is_valid():
            email = registerForm.cleaned_data['email']
            password = registerForm.cleaned_data['password']

            user = registerForm.save(commit=False)
            user.set_password(registerForm.cleaned_data['password'])
            user.save()

            # Authenticate user is registration successful
            user = authenticate(request, email=email, password=password)
            login(request, user)

            return redirect('home')
    else:
        registerForm = RegisterForm()

    return render(request, 'event_site/register.html', {'form': registerForm})


def loginview(request):
    if request.method == 'POST':
        # print(request.POST)
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(request, email=email, password=password)
        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            # Display an error message if authentication fails
            error_message = 'Invalid username or password'
            print(error_message)
            return render(request, 'event_site/login.html', {'error_message': error_message})
    else:
        return render(request, 'event_site/login.html')


@login_required(login_url='home')
def profile(request):
    current_user = User.objects.get(email=request.user)
    return render(request, 'event_site/profile.html', {'user': current_user})


@login_required
@user_passes_test(lambda u: u.is_superuser, login_url='home')
def addEvent(request):
    if request.method == 'POST':
        postData = request.POST
        image = request.FILES['event_poster']

        event = Event(
            host_id=request.user.email,
            venue=postData["venue"],
            start_date=postData["start_date"],
            end_date=postData["end_date"],
            post_date=date.today(),
            start_time=postData["start_time"],
            end_time=postData["end_time"],
            max_people_limit=postData["max_people_limit"],
            event_type=postData["event_type"],
            description=postData["description"],
            theme1=postData["theme1"],
            theme2=postData["theme2"],
            theme3=postData["theme3"],
            event_poster=image,
            event_name=postData["event_name"]
        )
        event.save()
        return redirect('updates')
    else:
        return render(request, 'event_site/addEvent.html')


@login_required
@user_passes_test(lambda u: u.is_superuser, login_url='home')
def addBlog(request):
    if request.method == 'POST':
        postData = request.POST
        image = request.FILES['image-input']

        blog = Blog(
            heading=postData["title"],
            date=date.today(),
            img=image,
            text=postData["desc"],
            link=postData["event-link"],
            tag1=postData["tag1"],
            tag2=postData["tag2"],
            tag3=postData["tag3"],
            tag4=postData["tag4"],
            tag5=postData["tag5"]
        )
        blog.save()
        return redirect('updates')
    else:
        return render(request, 'event_site/addBlog.html')


def blog(request, blog_id):
    current_blog = get_object_or_404(Blog, id=blog_id)
    return render(request, 'event_site/blog.html', {'blog': current_blog})


@login_required(login_url='home')
def logoutView(request):
    logout(request)
    return redirect('home')


@login_required
@user_passes_test(lambda u: u.is_superuser, login_url='home')
def adminPanel(request):
    return render(request, 'event_site/adminpanel.html')


@login_required
@user_passes_test(lambda u: u.is_superuser, login_url='home')
def admins(request):
    if request.method == 'POST':
        email = request.POST["email"]
        try:
            user = User.objects.get(email=email)
            user.is_superuser = True
            user.save()
            # return JsonResponse({'message': 'User is now a superuser.'})
        except:
            print("User not found.")
            # return JsonResponse({'error': 'User not found.'})

    superusers = User.objects.filter(is_superuser=True)
    emails = [user.email for user in superusers]
    return render(request, 'event_site/admins.html', {"admins": emails})


@login_required(login_url='home')
def editProfile(request):
    if request.method == 'POST':
        user_id = request.user.id
        user = get_object_or_404(User, id=user_id)

        for file in request.FILES.values():
            if file.content_type.startswith('image/'):
                user.profile_pic = request.FILES['image-input']
        # user.first_name = request.POST.get('name')
        # user.last_name = request.POST.get('name')
        user.email = request.POST.get('email')
        user.address = request.POST.get('address')
        user.phone = request.POST.get('phone')
        user.date_of_birth = request.POST.get('dob')
        user.user_bio = request.POST.get('bio')
        user.save()
        return redirect('profile')
    else:
        return render(request, 'event_site/editProfile.html')


def bookedEvents(request):
    return render(request, 'event_site/bookedEvents.html')
