from django.urls import path

from . import views

urlpatterns = [
    # Home Page
    path("", views.home, name="home"),
    path("home/", views.home, name="home"),

    # Updates Page
    path("updates/", views.updates, name="updates"),

    # Blog
    # path("blog/", views.blog, name="blog"),

    # Individual blog
    path('blog/<int:blog_id>/', views.blog, name='blog'),

    # Add Blog
    path("addblog/", views.addBlog, name="addBlog"),

    # Add Event
    path("addevent/", views.addEvent, name="addEvent"),

    # Register Page
    path("register/", views.register, name="register"),

    # Login Page
    path("login/", views.loginview, name="login"),

    # Profile Page
    path("profile/", views.profile, name="profile"),

    # Logout
    path("logout/", views.logoutView, name="logoutView"),

    # Admin Panel
    path("adminpanel/", views.adminPanel, name="adminPanel"),

    # Admins
    path("admins/", views.admins, name="admins"),

    # Edit Profile Pge
    path("editprofile/", views.editProfile, name="editProfile"),

    #Booked events
    path("bookedevents/", views.bookedEvents, name="bookedEvents"),
]
